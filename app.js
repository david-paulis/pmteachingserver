var createError = require('http-errors');
var express = require('express');
var path = require('path');
var cookieParser = require('cookie-parser');
var logger = require('morgan');

var indexRouter = require('./routes/index');
var usersRouter = require('./routes/users');
var testRouter = require('./routes/testRouter');
const uploadRouter = require('./routes/uploadRouter');
const paymentRouter = require('./routes/payment');
const promotionRouter = require('./routes/promotion');
var paypal = require('paypal-rest-sdk');


const mongoose = require('mongoose');
mongoose.Promise = require('bluebird');
const Tests = require('./models/test');
var app = express();

var passport = require('passport');
app.use(passport.initialize());

const url = 'mongodb://localhost:27017/pmpPrep';
const connect = mongoose.connect(url,{
  useMongoClient: true
});
connect.then((db)=>{
  console.log('Connect Correctly to Server ')}
  , err =>console.log(err));


// view engine setup
app.set('views', path.join(__dirname, 'views'));
app.set('view engine', 'jade');

app.use(logger('dev'));
app.use(express.json());
app.use(express.urlencoded({ extended: false }));
app.use(cookieParser());
app.use(express.static(path.join(__dirname, 'public')));

app.use('/', indexRouter);
app.use('/users', usersRouter);
app.use('/test', testRouter);
app.use('/imageUpload',uploadRouter);
app.use('/payment',paymentRouter);
app.use('/promotion',promotionRouter);
/*
paypal.configure({
  'mode': 'sandbox', //sandbox or live
  'client_id': 'AZy3tyxT5nM9Lu6RAWdbw8KeNkkbA6tfQV55J8rJ_SWRqsAp1NXV0Fi7LrqxY8B9JOJuiAO_l56zjpmc',
  'client_secret': 'EMsHifDJG6MAg454X_p1ivQSmzd5Bu1DocjN6MkJJNideaJJ4Rc6EeEtMcfdL__40CXSM-mAGu5ujRri'
});

app.get('/payment',(req,res,next)=>{
  var create_payment_json = {
    "intent": "sale",
    "payer": {
        "payment_method": "paypal"
    },
    "redirect_urls": {
        "return_url": "http://localhost:3000/executePayment",
        "cancel_url": "http://localhost:3000"
    },
    "transactions": [{
        "item_list": {
            "items": [{
                "name": "item",
                "sku": "item",
                "price": "1.00",
                "currency": "USD",
                "quantity": 1
            }]
        },
        "amount": {
            "currency": "USD",
            "total": "1.00"
        },
        "description": "This is the payment description."
    }]
};});
app.get("/executePayment",(req,res,next)=>{
  var payment_Id = req.query.paymentId;
 var payer_Id = req.query.payerID;
  var execute_payment_json = {
    "payer_id": payer_Id,
    "transactions": [{
        "amount": {
            "currency": "USD",
            "total": "1.00"
        }
    }]
};


var paymentId = payment_Id;

paypal.payment.execute(paymentId, execute_payment_json, function (error, payment) {
    if (error) {
        console.log(error.response);
        next(error);
    } else {
        console.log("Get Payment Response");
        console.log(JSON.stringify(payment));
    }
});
});

*/
  
  
// catch 404 and forward to error handler
app.use(function(req, res, next) {
  next(createError(404));
});

// error handler
app.use(function(err, req, res, next) {
  // set locals, only providing error in development
  res.locals.message = err.message;
  res.locals.error = req.app.get('env') === 'development' ? err : {};

  // render the error page
  res.status(err.status || 500);
  res.render('error');
});

module.exports = app;
