
var mongoose = require('mongoose');
var Schema = mongoose.Schema;

var payment = new Schema({
    userId: {
        type: String,
        required: true
    },
    testId: {
        type: String,
        required: true
    }
},


    { timestamps: true }

);

module.exports=mongoose.model("Payment",payment);