var mongoose = require('mongoose');
var Schema = mongoose.Schema;
var testType={
    _id:"",
    title:""
};
var Promotion = new Schema({
  price:{
      type:String,
      required:true
  },
  title:{
      type:String,
      required:true
  },
  test:[testType]
 
});
module.exports = mongoose.model('Promotion', Promotion);