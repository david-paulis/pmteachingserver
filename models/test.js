const mongoose = require('mongoose');
const Schema = mongoose.Schema;

// Will add the Currency type to the Mongoose Schema types
//require('mongoose-currency').loadType(mongoose);
//var Currency = mongoose.Types.Currency;



const quesionSchema = new Schema({
    process: {
        type: Number,
        required: true
    },
    other: {
        type: Number,
        required: true
    },
    answer1: {
        type: String,
        required: true
    },
    answer2: {
        type: String,
        required: true
    },
    answer3: {
        type: String,
        required: true
    },
    answer4: {
        type: String,
        required: true
    },
    correct: {
        type: Number,
        required: true
    },
    title: {
        type: String,
        required: true
    },

    explanation: {
        type: String,
        required: true
    }
});

const testSchema = new Schema({

    price: {
        type: Number,
        default: 0
    },
    free:{
        type:Boolean
        
    }
    ,
    title: {
        type: String,
        required: true
    },
    description: {
        type: String,
         required:true
    },
    time:{
        type:Number,
        required:true
    },


    questions: [quesionSchema]
},
    {
        timestamps: true
    }
);
var Tests = mongoose.model("Test", testSchema);
module.exports = Tests;