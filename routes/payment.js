
const mongoose = require('mongoose');
const express = require('express');
const bodyParser = require('body-parser');
const Payment = require('../models/payment');
const authenticate = require('../authenticate');
const paymentRouter = express.Router();
var cors = require('./cors');

paymentRouter.route("/")
.options(cors.corsWithOptions, (req, res) => { res.sendStatus(200); })
.post(cors.corsWithOptions,authenticate.verifyUser,(req,res,next)=>{
    Payment.create(req.body).then(()=>{
        res.statusCode = 200;
        res.setHeader('Content-Type', 'application/json');
        res.json({"status":true});
    },err=>next(err)).catch(err => next(err));
})
.get(cors.cors,authenticate.verifyUser,authenticate.verifyAdmin,(req,res,next)=>{

});
paymentRouter.route('/:userId')
.options(cors.corsWithOptions, (req, res) => { res.sendStatus(200); })
.get(cors.cors,authenticate.verifyUser,(req,res,next)=>{
Payment.find({userId:req.params.userId})
            .then((payments)=>{
                res.statusCode = 200;
                res.setHeader('Content-Type', 'application/json');
                res.json(payments);
            },err=>next(err)).catch(err=>next(err));
})
.delete(cors.corsWithOptions,authenticate.verifyUser,(req,res,next)=>{
    Payment.deleteOne({userId:req.params.userId,testId:req.body.testId})
            .then(res=>{
                res.statusCode = 200;
                res.setHeader('Content-Type', 'application/json');
                res.json({"status":true});
            },err=>next(err)).catch(err=>next(err));
});
module.exports=paymentRouter;
