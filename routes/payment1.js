const express = require('express');
const bodyParser = require('body-parser');
const authenticate = require('../authenticate');
var path = require('path');
var app = express();
var paypal = require('paypal-rest-sdk');
var cors = require('./cors');

// configure paypal with the credentials you got when you created your paypal app
paypal.configure({
    'mode': 'sandbox', //sandbox or live 
    'client_id': 'AXO5DDDZ3f2QlpMXVDKRyTmhPxdmmO25F-mhMsHyQ2IP4_l2o523H5SU2xCRp1BtYcz860eRn2asPZO8', // please provide your client id here 
    'client_secret': 'EGx-1dfK1bI0hbjfuOftjLgfcAuX-YSR_B9B0WENKuiiyWFCnu0atfxgFgj_VstafVpZA1rlOm2WGwFp' // provide your client secret here 
});


const paymentRouter = express.Router();

paymentRouter.route('/')
    .options(cors.corsWithOptions, (req, res) => { res.sendStatus(200); })
    .get(cors.cors, authenticate.verifyUser, (req, res, next) => {
        res.statusCode = 403;
        res.end('GET operation not supported on /payment');
    })
    .post(cors.corsWithOptions, authenticate.verifyUser, (req, res, next) => {
        price = req.body.price;
        description = req.body.description;
        // create payment object 
        var payment = {
            "intent": "authorize",
            "payer": {
                "payment_method": "paypal"
            },
            "redirect_urls": {
                "return_url": "http://192.168.1.2:3000/payment/success",
                "cancel_url": "http://192.168.1.2:3000/payment/error"
            },
            "transactions": [{
                "amount": {
                    "total": price,
                    "currency": "USD"
                },
                "description": description
            }]
        }

        // call the create Pay method 
        createPay(payment)
            .then((transaction) => {
                var id = transaction.id;
                var links = transaction.links;
                var counter = links.length;
                while (counter--) {
                    if (links[counter].method == 'REDIRECT') {
                        // redirect to paypal where user approves the transaction 
                        return res.redirect(links[counter].href);
                    }
                }
            })
            .catch((err) => {
                console.log(err);
                res.send(err);
            });

    })
    .put(authenticate.verifyUser, (req, res, next) => {

    })
    .delete(authenticate.verifyUser, (req, res, next) => {

    });

var createPay = (payment) => {
    return new Promise((resolve, reject) => {
        paypal.payment.create(payment, function (err, payment) {
            if (err) {
                reject(err);
            }
            else {
                resolve(payment);
            }
        });
    });
};

paymentRouter.route('success')
.options(cors.corsWithOptions, (req, res) => { res.sendStatus(200); })

    .get((req, res, next) => {
        res.statusCode = 200;
        res.send("get /success");
    })
    .post((req, res, next) => {
        res.statusCode = 200;
        res.send("post /success");
    });
paymentRouter.route('error')
    .get((req, res, next) => {
        res.statusCode = 200;
        res.send("get /error");
    })
    .post((req, res, next) => {
        res.statusCode = 200;
        res.send("post /error");
    });


module.exports = paymentRouter;