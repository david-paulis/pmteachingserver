const mongoose = require('mongoose');
const express = require('express');
const bodyParser = require('body-parser');
const Promotion = require('../models/promotion');
const authenticate = require('../authenticate');
const promotionRouter = express.Router();
var cors = require('./cors');

promotionRouter.route('/')
.options(cors.corsWithOptions, (req, res) => { res.sendStatus(200); })
.get(cors.cors,(req,res,next)=>{
   Promotion.find({})
        .then((promotion)=>{
            if(promotion.length > 0){
                res.statusCode = 200;
                res.setHeader('Content-Type', 'application/json');
                res.json(promotion );
            }else{
                res.statusCode = 200;
                res.setHeader('Content-Type', 'application/json');
                res.json(promotion );
            }
        }, err => next(err))
        .catch(err =>next(err));
})
.post(cors.corsWithOptions, authenticate.verifyUser, authenticate.verifyAdmin
    ,(req,res,next)=>{
        Promotion.create(req.body)
                .then((promotion)=>{
                    res.statusCode = 200;
                    res.setHeader('Content-Type', 'application/json');
                    res.json({"status":true});
                },err=> next(err))
                .catch(err=>next(err));
            

    }); 
    promotionRouter.options('/:promotionId',cors.corsWithOptions, (req, res) => { res.sendStatus(200); }) 
    promotionRouter.delete('/:promotionId',cors.corsWithOptions,authenticate.verifyUser
         ,authenticate.verifyAdmin ,(req,res,next)=>{
             Promotion.deleteOne({_id:req.params.promotionId})
                    .then((promo)=>{
                       Promotion.find({}).then(promotion=>{
                        res.statusCode = 200;
                        res.setHeader('Content-Type', 'application/json');
                        res.json(promotion ); 
                       });
                      
                    },err =>next(err))
                    .catch(err=>next(err));
         });  
module.exports = promotionRouter;