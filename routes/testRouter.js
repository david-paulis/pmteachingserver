
var createError = require('http-errors');
const mongoose = require('mongoose');
const express = require('express');
const bodyParser = require('body-parser');
//require('mongoose-currency').loadType(mongoose);
//var Currency = mongoose.Types.Currency;

const Tests = require('../models/test');
const testRouter = express.Router();
const authenticate = require('../authenticate');
testRouter.use(bodyParser.json());
var cors = require('./cors');



testRouter.route("/")
    .options(cors.corsWithOptions, (req, res) => { res.sendStatus(200); })
    .get(cors.cors, (req, res, next) => {
        Tests.find({}, { questions: 0 }).sort({price:1})
            .then((tests) => {
                if (tests != null) {
                    res.statusCode = 200;
                    res.setHeader('Content-Type', 'application/json');
                    res.json(tests);
                }
                else {
                    err = new Error('No test yet , Please add test! ');
                    err.status = 404;
                    return next(err);
                }
            }, (err) => next(err)).catch((err) => next(err));

    })
    .post(cors.corsWithOptions, authenticate.verifyUser, authenticate.verifyAdmin, (req, res, next) => {
        Tests.create(req.body)
            .then((test) => {
                res.statusCode = 200;
                res.setHeader('Content-Type', 'application/json');
                res.json(test);
            }, err => next(err))
            .catch(err => next(err));
    })
    .put(cors.corsWithOptions, authenticate.verifyUser, authenticate.verifyAdmin, (req, res, next) => {
        res.statusCode = 403;
        res.end('PUT operation not supported on /test');

    })
    .delete(cors.corsWithOptions, authenticate.verifyUser, authenticate.verifyAdmin, (req, res, next) => {
        res.statusCode = 403;
        res.end('DELETE operation not supported on /test');
    });
   
    testRouter.options('/free/:testId',cors.corsWithOptions, (req, res) => { res.sendStatus(200); })    
    testRouter.get('/free/:testId',cors.cors, (req, res, next) => {
    Tests.findById(req.params.testId)
        .then((test) => {
            if (test != null) {
                res.statusCode = 200;
                res.setHeader('Content-Type', 'application/json');
                res.json(test);
            }
        }, (err) => next(err)).catch((err) => next(err));

});

testRouter.route('/:testId')
    .options(cors.corsWithOptions, (req, res) => { res.sendStatus(200); })
    .get(cors.cors, authenticate.verifyUser, (req, res, next) => {
        Tests.findById(req.params.testId)
            .then((test) => {
                res.statusCode = 200;
                res.setHeader('Content-Type', 'application/json');
                res.json(test);
            }, err => next(err))
            .catch(err => next(err));
    })
    .post(cors.corsWithOptions, authenticate.verifyUser, authenticate.verifyAdmin, (req, res, next) => {
        res.statusCode = 403;
        res.end('POST operation not supported on /test/' + req.params.testId);
    })
    .put(cors.corsWithOptions, authenticate.verifyUser, authenticate.verifyAdmin, (req, res, next) => {
        Tests.findByIdAndUpdate(req.params.testId, { $set: req.body }, { new: true })
            .then((test) => {
                res.statusCode = 200;
                res.setHeader('Content-Type', 'application/json');
                res.end("test " + test._id + " updated.");
            }, err => next(err))
            .catch(err => next(err));
    })
    .delete(cors.corsWithOptions, authenticate.verifyUser, authenticate.verifyAdmin, (req, res, next) => {
        Tests.findByIdAndRemove(req.params.testId)
            .then((resp) => {
                res.statusCode = 200;
                res.setHeader('Content-Type', 'application/json');
                res.end("test  deleted.");
            }, err => next(err))
            .catch(err => next(err));
    });

testRouter.route('/:testId/question')
    .options(cors.corsWithOptions, (req, res) => { res.sendStatus(200); })
    .get(cors.cors, authenticate.verifyUser, authenticate.verifyAdmin, (req, res, next) => {
        Tests.findById(req.params.testId)
            .then(test => {
                if (test != null) {
                    res.statusCode = 200;
                    res.setHeader('Content-Type', 'application/json');
                    res.json(test.questions);
                }
                else {
                    err = new Error('test ' + req.params.testId + ' not found');
                    err.status = 404;
                    return next(err);
                }
            }, (err) => next(err))
            .catch((err) => next(err));
    })
    .post(cors.corsWithOptions, authenticate.verifyUser, authenticate.verifyAdmin, (req, res, next) => {
        Tests.findById(req.params.testId)
            .then(test => {
                if (test != null) {

                    test.questions.push(req.body);
                    test.save()
                        .then(test => {
                            res.statusCode = 200;
                            res.setHeader('Content-Type', 'application/json');
                            res.json(test);
                        }, err => next(err));
                } else {
                    err = new Error('test ' + req.params.testId + ' not found');
                    err.status = 404;
                    return next(err);
                }
            })
    });
/*testRouter.route('/:testId/freeQuestion/:questionId')
.options(cors.corsWithOptions, (req, res) => { res.sendStatus(200); })
.get(cors.cors, (req, res, next) => {
    Tests.findById(req.params.testId)
        .then(test => {
            if (test != null && test.questions.id(req.params.questionId) != null) {
                res.statusCode = 200;
                res.setHeader('Content-Type', 'application/json');
                res.json(test.questions.id(req.params.questionId));
            }
            else if (test == null) {
                err = new Error('Test ' + req.params.testId + ' not found');
                err.status = 404;
                return next(err);
            }
            else {
                err = new Error('Question ' + req.params.questionId + ' not found');
                err.status = 404;
                return next(err);
            }
        }, err => next(err))
        .catch(err => next(err));
});*/
testRouter.route('/:testId/question/:questionId')
    .options(cors.corsWithOptions, (req, res) => { res.sendStatus(200); })
    .get(cors.cors, authenticate.verifyUser, authenticate.verifyAdmin, (req, res, next) => {
        Tests.findById(req.params.testId)
            .then(test => {
                if (test != null && test.questions.id(req.params.questionId) != null) {
                    res.statusCode = 200;
                    res.setHeader('Content-Type', 'application/json');
                    res.json(test.questions.id(req.params.questionId));
                }
                else if (test == null) {
                    err = new Error('Test ' + req.params.testId + ' not found');
                    err.status = 404;
                    return next(err);
                }
                else {
                    err = new Error('Question ' + req.params.questionId + ' not found');
                    err.status = 404;
                    return next(err);
                }
            }, err => next(err))
            .catch(err => next(err));
    })
    .post(cors.corsWithOptions, (req, res, next) => {
        res.statusCode = 403;
        res.end('POST operation not supported on /test/' + req.params.testId
            + '/question/' + req.params.questionId);
    })
    .put(cors.corsWithOptions, authenticate.verifyUser, authenticate.verifyAdmin, (req, res, next) => {
        Tests.findById(req.params.testId)
            .then(test => {
                if (test != null && test.questions.id(req.params.questionId) != null) {
                    test.questions.id(req.params.questionId).title = req.body.title; test.questions.id(req.params.questionId).answer2 = req.body.answer2;
                    test.questions.id(req.params.questionId).answer1 = req.body.answer1; test.questions.id(req.params.questionId).answer3 = req.body.answer3;
                    test.questions.id(req.params.questionId).answer4 = req.body.answer4; test.questions.id(req.params.questionId).explanation = req.body.explanation;
                    test.questions.id(req.params.questionId).process = req.body.process; test.questions.id(req.params.questionId).other = req.body.other;
                    test.questions.id(req.params.questionId).correct = req.body.correct;
                    test.save().then((test) => {
                        res.statusCode = 200;
                        res.setHeader('Content-Type', 'application/json');
                        res.json(test);
                    }, err => next(err));

                }
                else if (test == null) {
                    err = new Error('Test ' + req.params.testId + ' not found');
                    err.status = 404;
                    return next(err);
                }
                else {
                    err = new Error('Question ' + req.params.questionId + ' not found');
                    err.status = 404;
                    return next(err);
                }
            }
            , err => next(err));
    })
    .delete(cors.corsWithOptions, authenticate.verifyUser, authenticate.verifyAdmin, (req, res, next) => {
        Tests.findById(req.params.testId)
            .then(test => {
                if (test != null && test.questions.id(req.params.questionId) != null) {
                    test.questions.id(req.params.questionId).remove();
                    test.save()
                        .then((test) => {
                            res.statusCode = 200;
                            res.setHeader('Content-Type', 'application/json');
                            res.json(test);
                        }, err => next(err));

                }
                else if (test == null) {
                    err = new Error('Test ' + req.params.testId + ' not found');
                    err.status = 404;
                    return next(err);
                }
                else {
                    err = new Error('Question ' + req.params.questionId + ' not found');
                    err.status = 404;
                    return next(err);
                }
            }
            , err => next(err));
    });

module.exports = testRouter;